#!/usr/bin/python3

import re
import pathlib

import asyncio
import aiohttp

bucket_url = "https://s3.amazonaws.com/capitalbikeshare-data"

data_folder = pathlib.Path("data/capital_bike_share/archive")
data_folder.mkdir(parents=True, exist_ok=True)


async def download_file(worker_name, session, queue):
    while True:
        filename = await queue.get()
        file_url = f"{bucket_url}/{filename}"
        resp = await session.get(file_url)

        print("Downloading", filename)

        file_path = data_folder / filename

        with file_path.open("wb") as f:
            f.write(await resp.content.read())

        print("Downloading done", filename)

        queue.task_done()


async def main():
    async with aiohttp.ClientSession() as session:
        queue = asyncio.Queue()

        file_pattern = re.compile("(\d+-\w+-\w+\.zip)")
        s3_files_board = await session.get(bucket_url)

        files = file_pattern.findall(await s3_files_board.text())

        for filename in files:
            queue.put_nowait(filename)

        tasks = [asyncio.create_task(
            download_file(f"worker-{i}", session, queue)
        ) for i in range(8)]

        await queue.join()

        for task in tasks:
            task.cancel()

        await asyncio.gather(*tasks, return_exceptions=True)


asyncio.run(main())
