#!/bin/sh

mkdir -p patched_libs

git -C patched_libs clone https://github.com/v01dXYZ/loaders.gl -b fix_browser_empty_module --depth 1
git -C patched_libs clone https://github.com/v01dXYZ/probe.gl -b fix_es6_comp --depth 1

yarn

yarn build

yarn upgrade @loaders.gl/load-utils @loaders.gl/core probe.gl
